#!/bin/bash

set -e
set -x

#export PICO_SDK_PATH=$PWD/pico-sdk
TARGET=pico_w
SSID="FAMHM_ENTEL"
PSW="flocris123"

mkdir -p build

pushd build

cmake -DPICO_BOARD=$TARGET -DWIFI_SSID=$SSID -DWIFI_PASSWORD=$PSW ..

popd
