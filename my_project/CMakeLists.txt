set ( BIN_NAME "my_project")

add_executable(${BIN_NAME}
        picow_blink.c
        )

target_link_libraries(${BIN_NAME}
        pico_stdlib              # for core functionality
        pico_cyw43_arch_none     # we need Wifi to access the GPIO, but we don't need anything else
        pico_cyw43_arch_lwip_threadsafe_background
        )

target_compile_definitions(${BIN_NAME} PRIVATE
        WIFI_SSID=\"${WIFI_SSID}\"
        WIFI_PASSWORD=\"${WIFI_PASSWORD}\"
        TEST_TCP_SERVER_IP=\"${TEST_TCP_SERVER_IP}\"
        )
target_include_directories(${BIN_NAME} PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}
        #${CMAKE_CURRENT_LIST_DIR}/.. # for our common lwipopts
        )

        # enable usb output, disable uart output
pico_enable_stdio_usb(${BIN_NAME} 1)
pico_enable_stdio_uart(${BIN_NAME} 0)

# create map/bin/hex file etc.
pico_add_extra_outputs(${BIN_NAME})

# add url via pico_set_program_url
example_auto_set_url(${BIN_NAME})
